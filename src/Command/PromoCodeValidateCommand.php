<?php

namespace App\Command;

use App\Service\CallApiService;
use App\Utils\CustomValidatorForCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class PromoCodeValidateCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';
    private CustomValidatorForCommand $validator;
    protected static $defaultDescription = 'Vérifier la validité d\'un code promo, et récupérer les offres associées';
    /**
     * @var SymfonyStyle
     */    private $io;
    /**
     * @var CallApiService
     */
    private  $service;

    public function __construct(CustomValidatorForCommand $validator,CallApiService $callApiService)
    {
        parent::__construct();
        $this->validator = $validator;
        $this->service = $callApiService;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('code', InputArgument::REQUIRED, 'Code Promo')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /**
     * Executed after configure() to initialize properties based on the input arguments and options.
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output):void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * Executed after initialize() znd before execute().
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output):void
    {
        //parent::interact($input, $output); // TODO: Change the autogenerated stub
        $this->io->section("VERIFIER LA VALIDITEE D'UN CODE PROMO");
        $this->enterCode($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $codePromo */
        $codePromo = $input->getArgument('code');
        $this->io->success("Création du fichier JSON");
        $this->io->block(json_encode($this->service->listOfferCompatible($codePromo)));
        return Command::SUCCESS;
    }

    /**
     * Sets the code promo
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function enterCode(InputInterface $input, OutputInterface $output):void
    {
        $helper = $this->getHelper('question');

        $codeQuestion = new Question("Code promo : ");

        $codeQuestion->setValidator([$this->validator, 'validateCodePromo']);

        $code = $helper->ask($input, $output, $codeQuestion);

        if(!($this->service->isValideCodePromo($code))){
            throw new RuntimeException(sprintf("Code promo n'est pas valide : %s", $code));
        }

        $input->setArgument('code', $code);
    }
}
