<?php


namespace App\Utils;


use Symfony\Component\Console\Exception\InvalidArgumentException;

class CustomValidatorForCommand
{
    /**
     * Validates a codePromo entered by the user in the CLI.
     * @param string|null $codePromoEntered
     * @return string
     */
    public function validateCodePromo(?string $codePromoEntered): string
    {
        if(empty($codePromoEntered)){
            throw new InvalidArgumentException("VEUILLEZ SAISIR UN CODE PROMO");
        }
        return $codePromoEntered;
    }
}
