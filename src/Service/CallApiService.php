<?php


namespace App\Service;


use DateTime;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    private $client;
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getPromoCodeList(): array
    {
        return $this->getApi('promoCodeList');
    }

    public function getOfferList(): array
    {
        return $this->getApi('offerList');
    }

    private function getApi(string $var):array
    {

        $response = $this->client->request(
            'GET',
            'https://601025826c21e10017050013.mockapi.io/ekwatest/' . $var
        );
        return $response->toArray();
    }

    /**
     * check the validity of the date format
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function validateDate($date, $format = 'Y-m-d'):bool
    {
        $d = DateTime::createFromFormat($format, $date);
        if(($d && $d->format($format) == $date))
        {
            return true;
        }
        throw new RuntimeException(sprintf("format de la date n'est pas valide : %s", $date));
    }

    /**
     * check the validity of the future date
     * @param string $time
     * @return bool
     */
    public static function isFuture(string $time):bool
    {
        if(self::validateDate($time))
        {
            return (strtotime($time) > time());
        }
        return false;
    }

    /**
     * check the validity of the promo code
     * @param string $codeP
     * @return bool
     */
    public function isValideCodePromo(string $codeP):bool
    {
        $TabPromoCodeList=$this->getPromoCodeList();
        foreach ($TabPromoCodeList as $item)
        {
            if(in_array($codeP, $item) && $this->isFuture($item['endDate']))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * return promo code data
     * @param string $codeP
     * @return array
     */
    public function returnCodePromoData(string $codeP):array
    {
        $Tab= $this->getPromoCodeList();
        foreach ($Tab as $key => $item)
        {
            if(in_array($codeP, $item))
            {
                return $item;
            }
        }
    }

    /**
     * return the data of the offers compatible with the promo code
     * @param string $codeP
     * @return array
     */
    public function returnOffreCompatibleData(string $codeP):array
    {
        $TabData =[];
        $TabOfferList=$this->getOfferList();
        $tab=[];
        foreach ($TabOfferList as $key => $item)
        {

            if(in_array($codeP, $item["validPromoCodeList"]))
            {
                $tab["name"]=$item["offerName"];
                $tab["type"]=$item["offerType"];
                $TabData[$key] =$tab;
            }
        }
        return $TabData;
    }

//    public function isCompatibleCodePromo(string $codeP):bool
//    {
//        $TabPromoCodeList=$this->getPromoCodeList();
//        foreach ($TabPromoCodeList as $item)
//        {
//            if(in_array($codeP, $item['validPromoCodeList']))
//            {
//                return true;
//            }
//        }
//        return false;
//    }

    /**
     * return the list of offers compatible with the promo code
     * @param string $codeP
     * @return array
     */
    public function listOfferCompatible(string $codeP):array
    {
        $TabReturn=[];
        $DataCodePromo = $this->returnCodePromoData($codeP);
        $DataOffreCompatible = $this->returnOffreCompatibleData($codeP);

        $TabReturn["promoCode"] = $DataCodePromo["code"];
        $TabReturn["endDate"] = $DataCodePromo["endDate"];
        $TabReturn["discountValue"] = $DataCodePromo["discountValue"];
        $TabReturn["compatibleOfferList"] = $DataOffreCompatible;

        return $TabReturn;

    }

}
