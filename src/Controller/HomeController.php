<?php


namespace App\Controller;


use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route ("/home", name="home")
     */
    public function index(CallApiService $callApiService)
    {
        //dd($callApiService->getOfferList());
        dd($callApiService->getPromoCodeList());
    }
}
