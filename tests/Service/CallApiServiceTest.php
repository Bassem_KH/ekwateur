<?php

namespace App\Tests\Service;

use App\Service\CallApiService;
use PHPUnit\Framework\TestCase;

class CallApiServiceTest extends TestCase
{
    public function testValidateDate()
    {
        $this->assertTrue(CallApiService::validateDate('2021-06-12'));
    }

    public function testisFuture()
    {
        $jour = date("Y-m-d");
        $maDate = strtotime($jour."+ 2 days");
        $this->assertTrue(CallApiService::isFuture(date("Y-m-d",$maDate)));
        $maDate = strtotime($jour."- 2 days");
        $this->assertFalse(CallApiService::isFuture(date("Y-m-d",$maDate)));
    }
}
